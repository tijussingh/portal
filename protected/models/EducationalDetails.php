<?php

/**
 * This is the model class for table "educational_details".
 *
 * The followings are the available columns in table 'educational_details':
 * @property integer $edu_det_id
 * @property string $degree
 * @property string $grade
 * @property string $stream
 * @property string $board
 */
class EducationalDetails extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'educational_details';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('degree, grade, stream, board', 'required'),
			array('degree', 'length', 'max'=>10),
			array('grade', 'length', 'max'=>5),
			array('stream', 'length', 'max'=>40),
			array('board', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('edu_det_id, degree, grade, stream, board', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'edu_det_id' => 'Edu Det',
			'degree' => 'Degree Held',
			'grade' => 'Percentage / Grade / SGPI / SGPA',
			'stream' => 'Branch',
			'board' => 'Board / University',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('edu_det_id',$this->edu_det_id);
		$criteria->compare('degree',$this->degree,true);
		$criteria->compare('grade',$this->grade,true);
		$criteria->compare('stream',$this->stream,true);
		$criteria->compare('board',$this->board,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EducationalDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
