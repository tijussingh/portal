<?php

/**
 * This is the model class for table "basic_info".
 *
 * The followings are the available columns in table 'basic_info':
 * @property integer $basic_info_id
 * @property string $DOB
 * @property string $gender
 * @property string $mobile_isd_code
 * @property integer $mobile_no
 * @property string $corr_address
 * @property string $permanent_address
 * @property string $website
 * @property string $hobbies
 * @property string $marital_status
 * @property integer $user_type
 * @property integer $status
 * @property string $first_login_date
 * @property string $last_profile_update_date
 * @property string $profile_pic
 */
class BasicInfo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'basic_info';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('DOB, gender, mobile_isd_code, mobile_no, corr_address, permanent_address, marital_status,  status', 'required'),
			array('mobile_no,  status', 'numerical', 'integerOnly'=>true),
			array('gender, marital_status', 'length', 'max'=>1),
			array('mobile_isd_code', 'length', 'max'=>4),
			array('corr_address, permanent_address', 'length', 'max'=>100),
			array('website', 'length', 'max'=>40),
			array('hobbies, profile_pic', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('basic_info_id, DOB, gender, mobile_isd_code, mobile_no, corr_address, permanent_address, website, hobbies, marital_status,  status, first_login_date, last_profile_update_date, profile_pic', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'basic_info_id' => 'Basic Info',
			'DOB' => 'Date of Birth',
			'gender' => 'Gender',
			'mobile_isd_code' => 'Mobile Isd Code',
			'mobile_no' => 'Mobile No',
			'corr_address' => 'Corr Address',
			'permanent_address' => 'Permanent Address',
			'website' => 'Website',
			'hobbies' => 'Hobbies',
			'marital_status' => 'Marital Status',
			'status' => 'Status',
			'first_login_date' => 'First Login Date',
			'last_profile_update_date' => 'Last Profile Update Date',
			'profile_pic' => 'Profile Pic',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('basic_info_id',$this->basic_info_id);
		$criteria->compare('DOB',$this->DOB,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('mobile_isd_code',$this->mobile_isd_code,true);
		$criteria->compare('mobile_no',$this->mobile_no);
		$criteria->compare('corr_address',$this->corr_address,true);
		$criteria->compare('permanent_address',$this->permanent_address,true);
		$criteria->compare('website',$this->website,true);
		$criteria->compare('hobbies',$this->hobbies,true);
		$criteria->compare('marital_status',$this->marital_status,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('first_login_date',$this->first_login_date,true);
		$criteria->compare('last_profile_update_date',$this->last_profile_update_date,true);
		$criteria->compare('profile_pic',$this->profile_pic,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BasicInfo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
