<?php

	class checkAccessController extends Controller
	{
		
		public function actionCheck()
		{
				$basicinfo = BasicInfo::model()->findByPk(Yii::app()->user->ID);
				$edudet = EducationalDetails::model()->findByPk(Yii::app()->user->ID);
				$techprof = TechnicalProficiency::model()->findByPk(Yii::app()->user->ID);
				$topiclist = TopicList::model()->findByPk(Yii::app()->user->ID);
				$projects = Projects::model()->findByPk(Yii::app()->user->ID);
				$we = WorkExperience::model()->findByPk(Yii::app()->user->ID);
				
				
				if(is_null($basicinfo))
					$this->redirect(array("/basicInfo/create"));
				else if(is_null($edudet))
					$this->redirect(array("/educationalDetails/create"));
				else if(is_null($techprof))
					$this->redirect(array("/technicalProficiency/create"));
				else if(is_null($topiclist))
					$this->redirect(array("/topicList/create"));
				else if(is_null($projects))
					$this->redirect(array("/projects/create"));
				else if(is_null($we))
					$this->redirect(array("/workExperience/create"));
				else if(is_null($edudet))
					$this->redirect(array("/educationalDetails/create"));
				else
					$this->redirect(array("/site/index"));
		}			
	}