<?php
/* @var $this AchievementsController */
/* @var $data Achievements */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('achievement_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->achievement_id), array('view', 'id'=>$data->achievement_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('level')); ?>:</b>
	<?php echo CHtml::encode($data->level); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />


</div>