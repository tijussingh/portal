<?php
/* @var $this AchievementsController */
/* @var $model Achievements */

$this->breadcrumbs=array(
	'Achievements'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Achievements', 'url'=>array('index')),
	array('label'=>'Manage Achievements', 'url'=>array('admin')),
);
?>

<h1>Create Achievements</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>