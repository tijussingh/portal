<?php
/* @var $this TopicListController */
/* @var $data TopicList */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('topic_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->topic_id), array('view', 'id'=>$data->topic_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('topic_name')); ?>:</b>
	<?php echo CHtml::encode($data->topic_name); ?>
	<br />


</div>