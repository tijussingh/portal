<?php
/* @var $this TopicListController */
/* @var $model TopicList */

$this->breadcrumbs=array(
	'Topic Lists'=>array('index'),
	$model->topic_id,
);

$this->menu=array(
	array('label'=>'List TopicList', 'url'=>array('index')),
	array('label'=>'Create TopicList', 'url'=>array('create')),
	array('label'=>'Update TopicList', 'url'=>array('update', 'id'=>$model->topic_id)),
	array('label'=>'Delete TopicList', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->topic_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TopicList', 'url'=>array('admin')),
);
?>

<h1>View TopicList #<?php echo $model->topic_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'topic_id',
		'topic_name',
	),
)); ?>
