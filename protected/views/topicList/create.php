<?php
/* @var $this TopicListController */
/* @var $model TopicList */

$this->breadcrumbs=array(
	'Topic Lists'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TopicList', 'url'=>array('index')),
	array('label'=>'Manage TopicList', 'url'=>array('admin')),
);
?>

<h1>Create TopicList</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>