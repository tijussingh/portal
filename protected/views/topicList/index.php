<?php
/* @var $this TopicListController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Topic Lists',
);

$this->menu=array(
	array('label'=>'Create TopicList', 'url'=>array('create')),
	array('label'=>'Manage TopicList', 'url'=>array('admin')),
);
?>

<h1>Topic Lists</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
