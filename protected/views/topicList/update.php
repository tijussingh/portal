<?php
/* @var $this TopicListController */
/* @var $model TopicList */

$this->breadcrumbs=array(
	'Topic Lists'=>array('index'),
	$model->topic_id=>array('view','id'=>$model->topic_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TopicList', 'url'=>array('index')),
	array('label'=>'Create TopicList', 'url'=>array('create')),
	array('label'=>'View TopicList', 'url'=>array('view', 'id'=>$model->topic_id)),
	array('label'=>'Manage TopicList', 'url'=>array('admin')),
);
?>

<h1>Update TopicList <?php echo $model->topic_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>