<?php
/* @var $this TopicListController */
/* @var $model TopicList */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'topic_id'); ?>
		<?php echo $form->textField($model,'topic_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'topic_name'); ?>
		<?php echo $form->textField($model,'topic_name',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->