<?php
/* @var $this TechnicalProficiencyController */
/* @var $data TechnicalProficiency */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('tech_prof_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->tech_prof_id), array('view', 'id'=>$data->tech_prof_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('skill')); ?>:</b>
	<?php echo CHtml::encode($data->skill); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('level')); ?>:</b>
	<?php echo CHtml::encode($data->level); ?>
	<br />


</div>