<?php
/* @var $this TechnicalProficiencyController */
/* @var $model TechnicalProficiency */

$this->breadcrumbs=array(
	'Technical Proficiencies'=>array('index'),
	$model->tech_prof_id=>array('view','id'=>$model->tech_prof_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TechnicalProficiency', 'url'=>array('index')),
	array('label'=>'Create TechnicalProficiency', 'url'=>array('create')),
	array('label'=>'View TechnicalProficiency', 'url'=>array('view', 'id'=>$model->tech_prof_id)),
	array('label'=>'Manage TechnicalProficiency', 'url'=>array('admin')),
);
?>

<h1>Update TechnicalProficiency <?php echo $model->tech_prof_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>