<?php
/* @var $this TechnicalProficiencyController */
/* @var $model TechnicalProficiency */

$this->breadcrumbs=array(
	'Technical Proficiencies'=>array('index'),
	$model->tech_prof_id,
);

$this->menu=array(
	array('label'=>'List TechnicalProficiency', 'url'=>array('index')),
	array('label'=>'Create TechnicalProficiency', 'url'=>array('create')),
	array('label'=>'Update TechnicalProficiency', 'url'=>array('update', 'id'=>$model->tech_prof_id)),
	array('label'=>'Delete TechnicalProficiency', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->tech_prof_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TechnicalProficiency', 'url'=>array('admin')),
);
?>

<h1>View TechnicalProficiency #<?php echo $model->tech_prof_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'tech_prof_id',
		'skill',
		'level',
	),
)); ?>
