<?php
/* @var $this TechnicalProficiencyController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Technical Proficiencies',
);

$this->menu=array(
	array('label'=>'Create TechnicalProficiency', 'url'=>array('create')),
	array('label'=>'Manage TechnicalProficiency', 'url'=>array('admin')),
);
?>

<h1>Technical Proficiencies</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
