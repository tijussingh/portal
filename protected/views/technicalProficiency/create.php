<?php
/* @var $this TechnicalProficiencyController */
/* @var $model TechnicalProficiency */

$this->breadcrumbs=array(
	'Technical Proficiencies'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TechnicalProficiency', 'url'=>array('index')),
	array('label'=>'Manage TechnicalProficiency', 'url'=>array('admin')),
);
?>

<h1>Create TechnicalProficiency</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>