<?php
/* @var $this EducationalDetailsController */
/* @var $model EducationalDetails */

$this->breadcrumbs=array(
	'Educational Details'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EducationalDetails', 'url'=>array('index')),
	array('label'=>'Manage EducationalDetails', 'url'=>array('admin')),
);
?>

<h1>Create EducationalDetails</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>