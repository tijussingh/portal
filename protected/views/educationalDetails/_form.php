<?php
/* @var $this EducationalDetailsController */
/* @var $model EducationalDetails */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'educational-details-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'degree'); ?>
		 <?php echo $form->dropDownList($model, 'degree', array('N'=>'Select','12'=>'12','DIPLOMA'=>'DIPLOMA','FE'=>'First Year Engineering','SE'=>'Second Year Engineering','TE'=>'Third Year Engineering','BE'=>'Fourth Year Engineering','O'=>'Others'));?>
		
		<?php echo $form->error($model,'degree'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'grade'); ?>
		<?php echo $form->textField($model,'grade',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'grade'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stream'); ?>
		 <?php echo $form->dropDownList($model, 'stream', array('N'=>'Select','1'=>'Studying','2'=>'Job Seeker','3'=>'Intern','4'=>'Employed','5'=>'Entrepreneur'));?>
		<?php echo $form->error($model,'stream'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'board'); ?>
		<?php echo $form->textField($model,'board',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'board'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->