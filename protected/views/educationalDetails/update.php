<?php
/* @var $this EducationalDetailsController */
/* @var $model EducationalDetails */

$this->breadcrumbs=array(
	'Educational Details'=>array('index'),
	$model->edu_det_id=>array('view','id'=>$model->edu_det_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EducationalDetails', 'url'=>array('index')),
	array('label'=>'Create EducationalDetails', 'url'=>array('create')),
	array('label'=>'View EducationalDetails', 'url'=>array('view', 'id'=>$model->edu_det_id)),
	array('label'=>'Manage EducationalDetails', 'url'=>array('admin')),
);
?>

<h1>Update EducationalDetails <?php echo $model->edu_det_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>