<?php
/* @var $this EducationalDetailsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Educational Details',
);

$this->menu=array(
	array('label'=>'Create EducationalDetails', 'url'=>array('create')),
	array('label'=>'Manage EducationalDetails', 'url'=>array('admin')),
);
?>

<h1>Educational Details</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
