<?php
/* @var $this EducationalDetailsController */
/* @var $data EducationalDetails */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('edu_det_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->edu_det_id), array('view', 'id'=>$data->edu_det_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('degree')); ?>:</b>
	<?php echo CHtml::encode($data->degree); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('grade')); ?>:</b>
	<?php echo CHtml::encode($data->grade); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stream')); ?>:</b>
	<?php echo CHtml::encode($data->stream); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('board')); ?>:</b>
	<?php echo CHtml::encode($data->board); ?>
	<br />


</div>