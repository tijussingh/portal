<?php
/* @var $this EducationalDetailsController */
/* @var $model EducationalDetails */

$this->breadcrumbs=array(
	'Educational Details'=>array('index'),
	$model->edu_det_id,
);

$this->menu=array(
	array('label'=>'List EducationalDetails', 'url'=>array('index')),
	array('label'=>'Create EducationalDetails', 'url'=>array('create')),
	array('label'=>'Update EducationalDetails', 'url'=>array('update', 'id'=>$model->edu_det_id)),
	array('label'=>'Delete EducationalDetails', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->edu_det_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EducationalDetails', 'url'=>array('admin')),
);
?>

<h1>View EducationalDetails #<?php echo $model->edu_det_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'edu_det_id',
		'degree',
		'grade',
		'stream',
		'board',
	),
)); ?>
