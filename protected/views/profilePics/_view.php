<?php
/* @var $this ProfilePicsController */
/* @var $data ProfilePics */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('profile_pic_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->profile_pic_id), array('view', 'id'=>$data->profile_pic_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('image_path')); ?>:</b>
	<?php echo CHtml::encode($data->image_path); ?>
	<br />


</div>