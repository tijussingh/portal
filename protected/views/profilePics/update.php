<?php
/* @var $this ProfilePicsController */
/* @var $model ProfilePics */

$this->breadcrumbs=array(
	'Profile Pics'=>array('index'),
	$model->profile_pic_id=>array('view','id'=>$model->profile_pic_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProfilePics', 'url'=>array('index')),
	array('label'=>'Create ProfilePics', 'url'=>array('create')),
	array('label'=>'View ProfilePics', 'url'=>array('view', 'id'=>$model->profile_pic_id)),
	array('label'=>'Manage ProfilePics', 'url'=>array('admin')),
);
?>

<h1>Update ProfilePics <?php echo $model->profile_pic_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>