<?php
/* @var $this ProfilePicsController */
/* @var $model ProfilePics */

$this->breadcrumbs=array(
	'Profile Pics'=>array('index'),
	$model->profile_pic_id,
);

$this->menu=array(
	array('label'=>'List ProfilePics', 'url'=>array('index')),
	array('label'=>'Create ProfilePics', 'url'=>array('create')),
	array('label'=>'Update ProfilePics', 'url'=>array('update', 'id'=>$model->profile_pic_id)),
	array('label'=>'Delete ProfilePics', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->profile_pic_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProfilePics', 'url'=>array('admin')),
);
?>

<h1>View ProfilePics #<?php echo $model->profile_pic_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'profile_pic_id',
		'image_path',
	),
)); ?>
