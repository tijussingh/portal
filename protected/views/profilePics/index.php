<?php
/* @var $this ProfilePicsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Profile Pics',
);

$this->menu=array(
	array('label'=>'Create ProfilePics', 'url'=>array('create')),
	array('label'=>'Manage ProfilePics', 'url'=>array('admin')),
);
?>

<h1>Profile Pics</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
