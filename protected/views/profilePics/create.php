<?php
/* @var $this ProfilePicsController */
/* @var $model ProfilePics */

$this->breadcrumbs=array(
	'Profile Pics'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ProfilePics', 'url'=>array('index')),
	array('label'=>'Manage ProfilePics', 'url'=>array('admin')),
);
?>

<h1>Create ProfilePics</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>