<?php
/* @var $this BasicInfoController */
/* @var $model BasicInfo */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'basic-info-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); 

?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
				
	<div class="row">
		<?php echo $form->labelEx($model,'DOB');?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
								
								'name'=>'BasicInfo[DOB]',
                            	'id'=>'BasicInfo_DOB',
                           	 	'options'=>array(
                            	'showAnim'=>'fold',
								'showButton'=>'on',
                            ),
                            'htmlOptions'=>array(
                            	 'style'=>'height:20px;'
                             ),
			)); ?>
		<?php echo $form->error($model,'DOB');?>
	</div> 
		
	<div class="row">
    	 <?php echo $form->labelEx($model,'gender'); ?>
		 <?php echo $form->radioButtonList($model, 'gender',
                    array(  'm' => 'Male',
                            'f' => 'Female' ),
                    array( 'labelOptions'=>array('style'=>'display:inline;width:150px;'), 'template'=>"{input} {label}", 'separator'=>'&nbsp;&nbsp;&nbsp;') );?> 
    	<?php echo $form->error($model,'gender'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'mobile_isd_code'); ?>
		<?php echo $form->textField($model,'mobile_isd_code',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'mobile_isd_code'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mobile_no'); ?>
		<?php echo $form->textField($model,'mobile_no',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'mobile_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'corr_address'); ?>
		<?php echo $form->textArea($model,'corr_address',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'corr_address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'permanent_address'); ?>
		<?php echo $form->textArea($model,'permanent_address',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'permanent_address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'website'); ?>
		<?php echo $form->textField($model,'website',array('size'=>20,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'website'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hobbies'); ?>
		<?php echo $form->textField($model,'hobbies',array('size'=>20,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'hobbies'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'marital_status'); ?>
		 <?php echo $form->dropDownList($model, 'marital_status', array('N'=>'Select','S'=>'Single', 'E'=>'Engaged','M'=>'Married', 'D'=>'Divorced','W'=>'Widowed'));?>
		<?php echo $form->error($model,'marital_status'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		 <?php echo $form->dropDownList($model, 'status', array('N'=>'Select','1'=>'Studying','2'=>'Job Seeker','3'=>'Intern','4'=>'Employed','5'=>'Entrepreneur'));?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	

	<div class="row">
		<?php echo $form->labelEx($model,'profile_pic'); ?>
		<?php echo $form->textField($model,'profile_pic',array('size'=>20,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'profile_pic'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->