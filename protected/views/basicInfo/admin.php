<?php
/* @var $this BasicInfoController */
/* @var $model BasicInfo */

$this->breadcrumbs=array(
	'Basic Infos'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List BasicInfo', 'url'=>array('index')),
	array('label'=>'Create BasicInfo', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#basic-info-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Basic Infos</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'basic-info-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'basic_info_id',
		'DOB',
		'gender',
		'mobile_isd_code',
		'mobile_no',
		'corr_address',
		/*
		'permanent_address',
		'website',
		'hobbies',
		'marital_status',
		'user_type',
		'status',
		'first_login_date',
		'last_profile_update_date',
		'profile_pic',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
