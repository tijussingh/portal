<?php
/* @var $this BasicInfoController */
/* @var $model BasicInfo */

$this->breadcrumbs=array(
	'Basic Infos'=>array('index'),
	$model->basic_info_id,
);

$this->menu=array(
	array('label'=>'List BasicInfo', 'url'=>array('index')),
	array('label'=>'Create BasicInfo', 'url'=>array('create')),
	array('label'=>'Update BasicInfo', 'url'=>array('update', 'id'=>$model->basic_info_id)),
	array('label'=>'Delete BasicInfo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->basic_info_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BasicInfo', 'url'=>array('admin')),
);
?>

<h1>View BasicInfo #<?php echo $model->basic_info_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'basic_info_id',
		'DOB',
		'gender',
		'mobile_isd_code',
		'mobile_no',
		'corr_address',
		'permanent_address',
		'website',
		'hobbies',
		'marital_status',
		'user_type',
		'status',
		'first_login_date',
		'last_profile_update_date',
		'profile_pic',
	),
)); ?>
