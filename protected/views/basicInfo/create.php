<?php
/* @var $this BasicInfoController */
/* @var $model BasicInfo */

$this->breadcrumbs=array(
	'Basic Infos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List BasicInfo', 'url'=>array('index')),
	array('label'=>'Manage BasicInfo', 'url'=>array('admin')),
);
?>

<h1>Create BasicInfo</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>