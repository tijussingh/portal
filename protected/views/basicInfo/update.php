<?php
/* @var $this BasicInfoController */
/* @var $model BasicInfo */

$this->breadcrumbs=array(
	'Basic Infos'=>array('index'),
	$model->basic_info_id=>array('view','id'=>$model->basic_info_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List BasicInfo', 'url'=>array('index')),
	array('label'=>'Create BasicInfo', 'url'=>array('create')),
	array('label'=>'View BasicInfo', 'url'=>array('view', 'id'=>$model->basic_info_id)),
	array('label'=>'Manage BasicInfo', 'url'=>array('admin')),
);
?>

<h1>Update BasicInfo <?php echo $model->basic_info_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>