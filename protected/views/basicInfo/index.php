<?php
/* @var $this BasicInfoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Basic Infos',
);

$this->menu=array(
	array('label'=>'Create BasicInfo', 'url'=>array('create')),
	array('label'=>'Manage BasicInfo', 'url'=>array('admin')),
);
?>

<h1>Basic Infos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
