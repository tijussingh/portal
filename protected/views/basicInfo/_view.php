<?php
/* @var $this BasicInfoController */
/* @var $data BasicInfo */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('basic_info_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->basic_info_id), array('view', 'id'=>$data->basic_info_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DOB')); ?>:</b>
	<?php echo CHtml::encode($data->DOB); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gender')); ?>:</b>
	<?php echo CHtml::encode($data->gender); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mobile_isd_code')); ?>:</b>
	<?php echo CHtml::encode($data->mobile_isd_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mobile_no')); ?>:</b>
	<?php echo CHtml::encode($data->mobile_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('corr_address')); ?>:</b>
	<?php echo CHtml::encode($data->corr_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('permanent_address')); ?>:</b>
	<?php echo CHtml::encode($data->permanent_address); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('website')); ?>:</b>
	<?php echo CHtml::encode($data->website); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hobbies')); ?>:</b>
	<?php echo CHtml::encode($data->hobbies); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('marital_status')); ?>:</b>
	<?php echo CHtml::encode($data->marital_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_type')); ?>:</b>
	<?php echo CHtml::encode($data->user_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_login_date')); ?>:</b>
	<?php echo CHtml::encode($data->first_login_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_profile_update_date')); ?>:</b>
	<?php echo CHtml::encode($data->last_profile_update_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profile_pic')); ?>:</b>
	<?php echo CHtml::encode($data->profile_pic); ?>
	<br />

	*/ ?>

</div>